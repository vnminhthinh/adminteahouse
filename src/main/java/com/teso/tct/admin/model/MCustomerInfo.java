package com.teso.tct.admin.model;

/**
 * @author Anh TST
 */
public class MCustomerInfo {

  private String name;
  private String birthday;
  private String phone;
  private String email;

  public MCustomerInfo() {
  }

  public MCustomerInfo(String name, String birthday, String phone, String email) {
    this.name = name;
    this.birthday = birthday;
    this.phone = phone;
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
}
