package com.teso.tct.admin.model;

public class MProfit {
    private String date;
    private String totalPrice;

    public MProfit() {
    }

    public MProfit(String date, String totalPrice) {
        this.date = date;
        this.totalPrice = totalPrice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }
}
