package com.teso.tct.admin.model;

/**
 * @author Anh TST
 */
public class MOrderDetail {

  private int stt;
  private String matHang;
  private long donGia;
  private int soLuong;
  private String ngayThu;
  private String ngayNhan;
  private String boSung;

  public MOrderDetail() {
  }

  public MOrderDetail(int stt, String matHang, long donGia, int soLuong, String ngayThu,
      String ngayNhan, String boSung) {
    this.stt = stt;
    this.matHang = matHang;
    this.donGia = donGia;
    this.soLuong = soLuong;
    this.ngayThu = ngayThu;
    this.ngayNhan = ngayNhan;
    this.boSung = boSung;
  }

  public int getStt() {
    return stt;
  }

  public void setStt(int stt) {
    this.stt = stt;
  }

  public String getMatHang() {
    return matHang;
  }

  public void setMatHang(String matHang) {
    this.matHang = matHang;
  }

  public long getDonGia() {
    return donGia;
  }

  public void setDonGia(long donGia) {
    this.donGia = donGia;
  }

  public int getSoLuong() {
    return soLuong;
  }

  public void setSoLuong(int soLuong) {
    this.soLuong = soLuong;
  }

  public String getNgayThu() {
    return ngayThu;
  }

  public void setNgayThu(String ngayThu) {
    this.ngayThu = ngayThu;
  }

  public String getNgayNhan() {
    return ngayNhan;
  }

  public void setNgayNhan(String ngayNhan) {
    this.ngayNhan = ngayNhan;
  }

  public String getBoSung() {
    return boSung;
  }

  public void setBoSung(String boSung) {
    this.boSung = boSung;
  }
}
