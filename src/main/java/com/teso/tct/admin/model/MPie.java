package com.teso.tct.admin.model;

public class MPie {
    private String item;
    private String totalPrice;
    private String id;

    public MPie() {
    }

    public MPie(String item, String totalPrice, String id) {
        this.item = item;
        this.totalPrice = totalPrice;
        this.id = id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
