package com.teso.tct.admin.model;

/**
 * @author Anh TST
 */
public class MItemInfo {

  private int id;
  private String refId;
  private String name;
  private String image;
  private int price;
  private String provider;
  private int quantity;

  public MItemInfo(String refId, String name, String image, int price, String provider,
      int quantity) {
    this.refId = refId;
    this.name = name;
    this.image = image;
    this.price = price;
    this.provider = provider;
    this.quantity = quantity;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getRefId() {
    return refId;
  }

  public void setRefId(String refId) {
    this.refId = refId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }
}
