package com.teso.tct.admin.model;

/**
 * @author Anh TST
 */
public class MPermissionInfo {

  private int id;
  private String name;
  private String rule;
  private boolean canRemove;

  public MPermissionInfo(int id, String name, String rule, boolean canRemove) {
    this.id = id;
    this.name = name;
    this.rule = rule;
    this.canRemove = canRemove;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRule() {
    return rule;
  }

  public void setRule(String rule) {
    this.rule = rule;
  }

  public boolean isCanRemove() {
    return canRemove;
  }

  public void setCanRemove(boolean canRemove) {
    this.canRemove = canRemove;
  }
}
