package com.teso.tct.admin.cache;

import com.teso.tct.admin.ent.AdminInfo;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Anh TST
 */
public class AdminInfoCA {

  private static AdminInfoCA instance = new AdminInfoCA();
  private static Map<String, AdminInfo> adminInfoMap;

  private AdminInfoCA() {
    adminInfoMap = new HashMap<>();
  }

  public static AdminInfoCA getInstance() {
    if (instance == null) {
      instance = new AdminInfoCA();
    }
    return instance;
  }

  public void addUserInfo(String email, AdminInfo userInfo) {
    adminInfoMap.put(email, userInfo);
  }

  public AdminInfo getAdminInfo(String email) {
    return adminInfoMap.get(email);
  }
}
