package com.teso.tct.admin.service;

import com.teso.tct.admin.db.PageRepository;
import com.teso.tct.admin.db.PageRepositoryProject;
import com.teso.tct.admin.ent.Item;
import com.teso.tct.admin.ent.Project;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PageServiceProject {

    @Autowired
    PageRepositoryProject recordRepository;
    public List<Project> findAll(PageRequest pageRequest){
        Page<Project> recordsPage = recordRepository.findAll(pageRequest);
        return recordsPage.getContent();
    }
}
