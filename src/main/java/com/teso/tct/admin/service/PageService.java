package com.teso.tct.admin.service;

import com.teso.tct.admin.db.PageRepository;
import com.teso.tct.admin.ent.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PageService {

    @Autowired
    PageRepository recordRepository;
    public List<Item> findAll(PageRequest pageRequest){
        Page<Item> recordsPage = recordRepository.findAll(pageRequest);
        return recordsPage.getContent();
    }
}
