package com.teso.tct.admin.ent;

import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author Anh TST
 */
@Entity
@Table(name = "customer_info")
public class CustomerInfo extends BaseModel {

  private String name;
  private Timestamp birthday;
  private String phone;
  private String email;

  @ManyToOne
  @JoinColumn(name = "admin_info_id")
  private AdminInfo adminInfo;

  public CustomerInfo() {
  }

  public CustomerInfo(String name, Timestamp birthday, String phone, String email) {
    this.name = name;
    this.birthday = birthday;
    this.phone = phone;
    this.email = email;
  }

  public CustomerInfo(String name, Timestamp birthday, String phone, String email,
      AdminInfo adminInfo) {
    this.name = name;
    this.birthday = birthday;
    this.phone = phone;
    this.email = email;
    this.adminInfo = adminInfo;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void setName(String name) {
    this.name = name;
  }

  public Timestamp getBirthday() {
    return birthday;
  }

  public void setBirthday(Timestamp birthday) {
    this.birthday = birthday;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public AdminInfo getAdminInfo() {
    return adminInfo;
  }

  public void setAdminInfo(AdminInfo adminInfo) {
    this.adminInfo = adminInfo;
  }
}
