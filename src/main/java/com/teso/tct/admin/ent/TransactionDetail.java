package com.teso.tct.admin.ent;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.UUID;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "transactionDetail")

public class TransactionDetail {

    @Embeddable
    public static  class TransactionDetailPK  implements Serializable {
        private Long transactionId;
        private String itemId;

        public TransactionDetailPK() {
        }

        public TransactionDetailPK(Long transactionId, String itemId) {
            this.transactionId = transactionId;
            this.itemId = itemId;
        }

        public Long getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(Long transactionId) {
            this.transactionId = transactionId;
        }

        public String getItemId() {
            return itemId;
        }

        public void setItemId(String itemId) {
            this.itemId = itemId;
        }

    }
    @EmbeddedId
    private TransactionDetailPK transactionDetailId;

    @MapsId("TransactionId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "transactionId", referencedColumnName = "id")
    private Transaction transaction;

    @MapsId("ItemId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "itemId", referencedColumnName = "id")
    private Item item;

    private int quantity;
    private String price;

    public TransactionDetail() {
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public TransactionDetailPK getTransactionDetailId() {
        return transactionDetailId;
    }

    public void setTransactionDetailId(TransactionDetailPK transactionDetailId) {
        this.transactionDetailId = transactionDetailId;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public TransactionDetail(TransactionDetailPK transactionDetailId, Transaction transaction, Item item, int quantity, String price) {
        this.transactionDetailId = transactionDetailId;
        this.transaction = transaction;
        this.item = item;
        this.quantity = quantity;
        this.price = price;
    }

}

