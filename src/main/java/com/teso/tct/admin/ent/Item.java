package com.teso.tct.admin.ent;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "item")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String imgUrl;
    private String title;
    @Lob
    private String description;
    @Lob
    private String content;
    private boolean isShowHomePage;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date publishedTime;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdTime;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedTime;

    private String price;

    public Item() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Item(String imgUrl, String title, String description, String content, boolean isShowHomePage, Project project, Date publishedTime, Date createdTime, Date updatedTime, String price) {
        this.imgUrl = imgUrl;
        this.title = title;
        this.description = description;
        this.content = content;
        this.isShowHomePage = isShowHomePage;
        this.project = project;
        this.publishedTime = publishedTime;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Date getPublishedTime() {
        return publishedTime;
    }

    public void setPublishedTime(Date publishedTime) {
        this.publishedTime = publishedTime;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public boolean isShowHomePage() {
        return isShowHomePage;
    }

    public void setShowHomePage(boolean showHomePage) {
        isShowHomePage = showHomePage;
    }

    @PrePersist
    void onCreate() {
        this.setCreatedTime(new Date(System.currentTimeMillis()));
        this.setPublishedTime(new Date(System.currentTimeMillis()));
    }

    @PreUpdate
    void onPersist() {
        this.setUpdatedTime(new Date(System.currentTimeMillis()));
    }
}

