package com.teso.tct.admin.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "admin_info")
public class AdminInfo extends BaseModel {

  @Column(columnDefinition = "NVARCHAR(255)")
  private String name = "Anonymous";

  private String email;

  private String avatar;

  private String role;

  @ManyToOne
  @JoinColumn(name = "permission_info_id", nullable = false)
  private PermissionInfo permissionInfo;


  public AdminInfo() {
  }

  public AdminInfo(String email, PermissionInfo permissionInfo) {
    this.email = email;
    this.permissionInfo = permissionInfo;
  }

  public AdminInfo(String email, String role, PermissionInfo permissionInfo) {
    this.email = email;
    this.role = role;
    this.permissionInfo = permissionInfo;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAvatar() {
    return avatar;
  }

  public void setAvatar(String avatar) {
    this.avatar = avatar;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public PermissionInfo getPermissionInfo() {
    return permissionInfo;
  }

  public void setPermissionInfo(PermissionInfo permissionInfo) {
    this.permissionInfo = permissionInfo;
  }
}
