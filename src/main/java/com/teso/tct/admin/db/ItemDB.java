package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.Item;
import org.springframework.data.jpa.repository.JpaRepository;
public interface ItemDB  extends JpaRepository<Item, Long> {

}
