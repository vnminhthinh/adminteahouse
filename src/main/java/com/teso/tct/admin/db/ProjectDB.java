package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.Project;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectDB  extends JpaRepository<Project, String> {
}
