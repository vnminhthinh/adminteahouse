package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuRepo extends JpaRepository<Menu, String> {
}
