package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.AdminInfo;
import com.teso.tct.admin.ent.CustomerInfo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * CustomerInfo Repositories
 *
 * @author Anh TST
 */
@Repository
public interface CustomerInfoDB extends JpaRepository<CustomerInfo, Integer> {

  @Query("SELECT p FROM CustomerInfo p WHERE p.adminInfo = :adminInfo")
  List<CustomerInfo> findAllByAdminInfoId(@Param("adminInfo") AdminInfo adminInfo);
}
