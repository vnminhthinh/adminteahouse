package com.teso.tct.admin.config;

import com.teso.framework.common.Config;

public class ConfigInfo {
    public static final String SPRING_BOOT_CONFIG = Config.getParam("spring_boot", "conf_path");

    public static final String LOGIN_URL = Config.getParam("service_url", "login");
    public static final String COMPANY_NAME = Config.getParam("company_name", "name");
    public static final String CLIENT_ID = Config.getParam("client_id", "id");
    public static final String UPLOAD_PATH = Config.getParam("upload_path", "folder");
    public static final String UPLOAD_FOLDER = Config.getParam("upload_folder", "name");
    public static final String UPLOAD_URL = Config.getParam("upload_url", "upload_name");
    public static final String LIST_STATUS = Config.getParam("status", "statusList");
    public static final String PAGE_SIZE = Config.getParam("page", "pageSize");
    public static final String PRICE = Config.getParam("conf_price", "price");
    public static final String MENU = Config.getParam("conf_menu", "menu");
    public static final String TRACSACTION = Config.getParam("conf_transaction", "transaction");
    public static final String API_URL = Config.getParam("api_url", "url");
    public static final String IMAGE = Config.getParam("conf_menu", "img");
    public static final String DESCRIPTION_PROJECT = Config.getParam("conf_project", "des");
    public static final String DESCRIPTION_ITEM = Config.getParam("conf_item", "des");
    public static final String CONTENT_ITEM = Config.getParam("conf_item", "content");
    public static final String TITLE_ITEM = Config.getParam("conf_item", "title");
    public static final String EMAIL=Config.getParam("conf_email","email");


}
