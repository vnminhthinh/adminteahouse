package com.teso.tct.admin.api;

import com.teso.framework.common.Config;
import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.PostsDB;
import com.teso.tct.admin.ent.Posts;
import com.teso.tct.admin.ent.Project;
import com.teso.tct.admin.model.MProject;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.utils.ApiAuthenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;


@Controller
public class PostsController {
    private final StorageService storageService;
    @Autowired
    private PostsDB postsDB;

    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private final String UPLOAD_FOLDER = ConfigInfo.UPLOAD_FOLDER;
    @Autowired
    public PostsController(StorageService storageService) {
        this.storageService = storageService;
    }

    @RequestMapping("/posts")
    public String items(Model model, HttpServletRequest request, HttpServletResponse response) {
        model.addAttribute("page_content", "posts.ftl");
        List<Posts> postsList = postsDB.findAll().stream().map(p ->
                new Posts(p.getId(), p.getTitle(), p.getThumbnail(), p.getDescription(),p.getContent())).collect(Collectors.toList());

        model.addAttribute("postsList", postsList);
        model.addAttribute("upload_url", UPLOAD_URL);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/add_posts")
    public String addProject(Model model, HttpServletRequest request,
                             HttpServletResponse response) {
        model.addAttribute("page_content", "add_posts.ftl");


        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");

    }

    @PostMapping("createPosts")
    public void createProject(Model model, @RequestParam("thumbnail") MultipartFile files, HttpServletRequest request, HttpServletResponse response) throws IOException {
        storageService.store(files);
        String id = request.getParameter("id");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String content= request.getParameter("editor1");
        Posts posts = new Posts();
        posts.setId(id);
        posts.setTitle(title);
        posts.setDescription(description);
        posts.setThumbnail(UPLOAD_FOLDER+files.getOriginalFilename());
        posts.setContent(content);
        postsDB.save(posts);
        System.out.println(content);
        response.sendRedirect("/add_posts");
    }
    @RequestMapping("/removePosts")
    public void removeProject(HttpServletResponse response, @RequestParam(name = "id", defaultValue = "0") String id)
            throws IOException {
        postsDB.deleteById(id);
        response.sendRedirect("/posts");
    }
    @RequestMapping("/imagebrowser")
    public String browser(Model model, HttpServletRequest request,
                             HttpServletResponse response) {
        model.addAttribute("url_img", "asd");
        return "browser";

    }
    @PostMapping("upload")
    public void upload(Model model, @RequestParam("flFileUpload") MultipartFile files, HttpServletRequest request, HttpServletResponse response) throws IOException {
        storageService.store(files);
//        String
//        String id = request.getParameter("id");
//        String title = request.getParameter("title");
//        String description = request.getParameter("description");
//        String content= request.getParameter("editor1");
//        Posts posts = new Posts();
//        posts.setId(id);
//        posts.setTitle(title);
//        posts.setDescription(description);
//        posts.setThumbnail(UPLOAD+files.getOriginalFilename());
//        posts.setContent(content);
//        postsDB.save(posts);
        String content= UPLOAD_FOLDER+files.getOriginalFilename();
        model.addAttribute("url_img", content);
        System.out.println("Editor content: " + content);
        response.sendRedirect("/imagebrowser");
    }
}
