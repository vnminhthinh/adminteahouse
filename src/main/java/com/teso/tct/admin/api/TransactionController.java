package com.teso.tct.admin.api;

import com.teso.framework.utils.ConvertUtils;
import com.teso.framework.utils.DateTimeUtils;
import com.teso.framework.utils.JSONUtil;
import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.ItemDB;
import com.teso.tct.admin.db.TransactionDetailRepo;
import com.teso.tct.admin.db.TransactionRepo;
import com.teso.tct.admin.ent.Transaction;
import com.teso.tct.admin.ent.TransactionDetail;
import com.teso.tct.admin.model.MPie;
import com.teso.tct.admin.model.MProfit;
import com.teso.tct.admin.service.PageServiceTransaction;
import com.teso.tct.admin.utils.ApiAuthenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Controller
public class TransactionController {
    @Autowired
    private ItemDB itemDB;
    @Autowired
    private TransactionRepo transactionRepo;
    @Autowired
    private TransactionDetailRepo transactionDetailRepo;
    @Autowired
    PageServiceTransaction pageServiceTransaction;
    private String statusList = ConfigInfo.LIST_STATUS;
    private String[] status;
    private List<String> listStatus;
    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private List<MProfit> listProfit;
    private String date;
    private String startDate = ConvertUtils.toString(DateTimeUtils.addMonths(-1), "dd/MM/yyyy");
    private String endDate = DateTimeUtils.getNow("dd/MM/yyyy");
    private boolean check = false;
    private int totalPrice = 0;
    private int price = 0;
    private int priceItem = 0;
    private MProfit profit;
    private MPie pie;
    private List<MPie> listPie;
    private List<MPie> mPieList;
    private DecimalFormat decimalFormat = new DecimalFormat("#.00");
    private HashMap<String, MPie> mPies;
    private boolean EMAIL= ConvertUtils.toBoolean(ConfigInfo.EMAIL);
    @RequestMapping("/transaction")
    public String transaction(Model model, @RequestParam(name = "page", defaultValue = "1") int page) {
        model.addAttribute("page_content", "transaction.ftl");
        model.addAttribute("email_conf",EMAIL);
        int pageSize = 10;
        List<Transaction> transactionList = transactionRepo.findAll();
        List<Transaction> transactions = pageServiceTransaction.findAllTransaction(PageRequest.of(page - 1, pageSize));
        listStatus = new ArrayList<>();
        status = statusList.split("-");
        for (int i = 0; i < status.length; i++) {
            listStatus.add(status[i]);
        }
        int i = transactionList.size();
        int j = i % pageSize;
        int k = i / pageSize;
        if (j > 0) {
            k++;
        }

        model.addAttribute("transactionList", transactions);
        model.addAttribute("listStatus", listStatus);
        model.addAttribute("page", k);
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("prePage", page - 1);
        model.addAttribute("currentPage", page);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/updateStatus")
    public void updateStatus(HttpServletRequest request, HttpServletResponse response,
                             @RequestParam(name = "id", defaultValue = "0") String id,
                             @RequestParam(name = "status", defaultValue = "Đã Giao") String status) throws IOException {

        Transaction transaction = transactionRepo.findItemById(id);
        transaction.setStatus(status);
        transactionRepo.save(transaction);
        response.sendRedirect("/transaction");

    }

    @RequestMapping("detailTransaction")
    public String detailTransaction(Model model, @RequestParam(name = "id", defaultValue = "0") String id) {
        model.addAttribute("page_content", "transaction_detail.ftl");
        List<TransactionDetail> details = transactionDetailRepo.findByTransactionId(id);
        model.addAttribute("transactionDetails", details);
        model.addAttribute("upload_url", UPLOAD_URL);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }
    @RequestMapping("profits")
    public String profits(Model model) {
        model.addAttribute("page_content", "profit.ftl");
        int year = DateTimeUtils.getYear(DateTimeUtils.getNow());
        model.addAttribute("year", String.valueOf(year));
        model.addAttribute("dateOld", startDate);
        model.addAttribute("dateNow", endDate);

        calculartorPrice(startDate, endDate);
        mPieList = new ArrayList<>();
        mPieList = new ArrayList<>(mPies.values());
        model.addAttribute("listPie", mPieList);
        model.addAttribute("list", listProfit);
        model.addAttribute("date", startDate);
        model.addAttribute("datenew", endDate);
        model.addAttribute("totalPrice", decimalFormat.format(totalPrice / 1000000.0));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    public void calculartorPrice(String sdate, String edate) {
        listProfit = new ArrayList<>();
        listPie = new ArrayList<>();
        List<Transaction> transactionList = transactionRepo.findAll();
        long ab = ConvertUtils.toDateTime(edate, "dd/MM/yyyy").getTime() - ConvertUtils.toDateTime(sdate, "dd/MM/yyyy").getTime();
        if (ab >= 0) {
            long d = TimeUnit.MILLISECONDS.toDays(ab);
            for (int i = 0; i <= d; i++) {
                profit = new MProfit();
                date = "";
                price = 0;
                date = ConvertUtils.toString(DateTimeUtils.addDays(ConvertUtils.toDateTime(sdate, "dd/MM/yyyy"), i), "dd/MM/yyyy");
                profit.setDate(date);

                for (int j = 0; j < transactionList.size(); j++) {
                    String[] transactionD = transactionList.get(j).getCreated_date().toString().split(" ");
                    if (DateTimeUtils.compareDate(ConvertUtils.toDateTime(date, "dd/MM/yyyy"), ConvertUtils.toDateTime(ConvertUtils.toDateTime(transactionD[0], "yyyy-MM-dd"), "dd/MM/yyyy")) == 0) {
                        price += Integer.parseInt(transactionList.get(j).getTotalPrice());
                        if (transactionList.get(j).getTransactionDetails().size() != 0) {
                            Set<TransactionDetail> sTransDetail = transactionList.get(j).getTransactionDetails();
                            for (TransactionDetail t : sTransDetail) {
                                pie = new MPie();
                                priceItem = 0;
                                pie.setId(t.getItem().getId());
                                pie.setItem(t.getItem().getTitle());
                                priceItem = Integer.parseInt(t.getPrice());
                                pie.setTotalPrice(t.getPrice());
                                listPie.add(pie);
                            }

                        }
                    } else {
                        price += 0;
                    }
                }
                profit.setTotalPrice(decimalFormat.format(price / 1000000.0));
                listProfit.add(profit);
            }
            totalPrice = 0;
            for (int i = 0; i < transactionList.size(); i++) {
                String[] transactionDate = transactionList.get(i).getCreated_date().toString().split(" ");
                if (DateTimeUtils.compareDate(ConvertUtils.toDateTime(sdate, "dd/MM/yyyy"), ConvertUtils.toDateTime(ConvertUtils.toDateTime(transactionDate[0], "yyyy-MM-dd"), "dd/MM/yyyy")) == -1
                        && DateTimeUtils.compareDate(ConvertUtils.toDateTime(edate, "dd/MM/yyyy"), ConvertUtils.toDateTime(ConvertUtils.toDateTime(transactionDate[0], "yyyy-MM-dd"), "dd/MM/yyyy")) == 1
                        || DateTimeUtils.compareDate(ConvertUtils.toDateTime(edate, "dd/MM/yyyy"), ConvertUtils.toDateTime(ConvertUtils.toDateTime(transactionDate[0], "yyyy-MM-dd"), "dd/MM/yyyy")) == 0
                        || DateTimeUtils.compareDate(ConvertUtils.toDateTime(sdate, "dd/MM/yyyy"), ConvertUtils.toDateTime(ConvertUtils.toDateTime(transactionDate[0], "yyyy-MM-dd"), "dd/MM/yyyy")) == 0) {
                    totalPrice += Integer.parseInt(transactionList.get(i).getTotalPrice());
                }
            }
        }
        mPies = new HashMap();
        for (int i = 0; i < listPie.size(); i++) {

            MPie pie = listPie.get(i);
            MPie cPie = mPies.get(pie.getId());
            if (cPie == null) {

                mPies.put(pie.getId(), pie);
            } else {
                double prevPrice = ConvertUtils.toDouble(cPie.getTotalPrice());
                double curPrice = ConvertUtils.toDouble(pie.getTotalPrice());
                double a = prevPrice + curPrice;
                cPie.setTotalPrice(String.valueOf(a));
            }

        }
    }

    @PostMapping("/profits")
    public void profit(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        startDate = request.getParameter("date");
        endDate = request.getParameter("datenew");
        model.addAttribute("dateOld", startDate);
        model.addAttribute("dateNow", endDate);
        totalPrice = 0;
        calculartorPrice(startDate, endDate);
        mPieList = new ArrayList<>();
        mPieList = new ArrayList<>(mPies.values());
        model.addAttribute("listPie", mPieList);
        model.addAttribute("list", listProfit);
        model.addAttribute("date", startDate);
        model.addAttribute("datenew", endDate);
        model.addAttribute("totalPrice", decimalFormat.format(totalPrice / 1000000.0));
        response.sendRedirect("/profits");
    }

    @PostMapping("/profits_now")
    public void profitNow(Model model, HttpServletResponse response) throws IOException {
        startDate = DateTimeUtils.getNow("dd/MM/yyyy");
        endDate = DateTimeUtils.getNow("dd/MM/yyyy");
        model.addAttribute("dateOld", startDate);
        model.addAttribute("dateNow", endDate);
        totalPrice = 0;
        calculartorPrice(startDate, endDate);
        mPieList = new ArrayList<>();
        mPieList = new ArrayList<>(mPies.values());
        model.addAttribute("listPie", mPieList);
        model.addAttribute("list", listProfit);
        model.addAttribute("date", startDate);
        model.addAttribute("datenew", endDate);
        model.addAttribute("totalPrice", decimalFormat.format(totalPrice / 1000000.0));
        response.sendRedirect("/profits");
    }

    @PostMapping("/profits_yesterday")
    public void profitYesterday(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        startDate = ConvertUtils.toString(DateTimeUtils.addDays(-1), "dd/MM/yyyy");
        endDate = ConvertUtils.toString(DateTimeUtils.addDays(-1), "dd/MM/yyyy");
        model.addAttribute("dateOld", startDate);
        model.addAttribute("dateNow", endDate);
        totalPrice = 0;
        calculartorPrice(startDate, endDate);
        mPieList = new ArrayList<>();
        mPieList = new ArrayList<>(mPies.values());
        model.addAttribute("listPie", mPieList);
        model.addAttribute("list", listProfit);
        model.addAttribute("date", startDate);
        model.addAttribute("datenew", endDate);
        model.addAttribute("totalPrice", decimalFormat.format(totalPrice / 1000000.0));
        response.sendRedirect("/profits");
    }

    @PostMapping("/profits_week")
    public void profitWeek(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        startDate = ConvertUtils.toString(DateTimeUtils.addDays(-7), "dd/MM/yyyy");
        endDate = DateTimeUtils.getNow("dd/MM/yyyy");
        model.addAttribute("dateOld", startDate);
        model.addAttribute("dateNow", endDate);
        totalPrice = 0;
        listProfit = new ArrayList<>();
        calculartorPrice(startDate, endDate);
        mPieList = new ArrayList<>();
        mPieList = new ArrayList<>(mPies.values());
        model.addAttribute("listPie", mPieList);
        model.addAttribute("list", listProfit);
        model.addAttribute("date", startDate);
        model.addAttribute("datenew", endDate);
        model.addAttribute("totalPrice", decimalFormat.format(totalPrice / 1000000.0));
        response.sendRedirect("/profits");
    }

    @PostMapping("/profits_month")
    public void profitMonth(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        startDate = ConvertUtils.toString(DateTimeUtils.addMonths(-1), "dd/MM/yyyy");
        endDate = DateTimeUtils.getNow("dd/MM/yyyy");
        model.addAttribute("dateOld", startDate);
        model.addAttribute("dateNow", endDate);
        totalPrice = 0;
        listProfit = new ArrayList<>();
        calculartorPrice(startDate, endDate);
        mPieList = new ArrayList<>();
        mPieList = new ArrayList<>(mPies.values());
        model.addAttribute("listPie", mPieList);
        model.addAttribute("list", listProfit);
        model.addAttribute("date", startDate);
        model.addAttribute("datenew", endDate);
        model.addAttribute("totalPrice", decimalFormat.format(totalPrice / 1000000.0));
        response.sendRedirect("/profits");
    }


//    @PostMapping("/profits_six_month")
//    public void profitSixMonth(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
//        lcdate = LocalDate.now().minusMonths(6);
//        lcdate_new = LocalDate.now();
//        model.addAttribute("dateOld", lcdate_new);
//        model.addAttribute("dateNow", lcdate_new);
//        totalPrice = 0;
//        List<Transaction> transactionList = transactionRepo.findAll();
//        for (int i = 0; i < transactionList.size(); i++) {
//            String[] transactionDate = transactionList.get(i).getCreated_date().toString().split(" ");
//            LocalDate localDate = LocalDate.parse(transactionDate[0]);
//            if (localDate.isAfter(lcdate) && localDate.isBefore(lcdate_new) || localDate.isEqual(lcdate_new) || localDate.isEqual(lcdate)) {
//                totalPrice += Integer.parseInt(transactionList.get(i).getTotalPrice());
//            }
//        }
//        check = true;
//        model.addAttribute("date", lcdate_new);
//        model.addAttribute("datenew", lcdate_new);
//       model.addAttribute("totalPrice", decimalFormat.format( totalPrice / 1000000.0));
//        response.sendRedirect("/profits");
//    }
//
//    @PostMapping("/profits_year")
//    public void profitYear(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
//        lcdate = LocalDate.now().minusYears(1);
//        lcdate_new = LocalDate.now();
//        model.addAttribute("dateOld", lcdate_new);
//        model.addAttribute("dateNow", lcdate_new);
//        totalPrice = 0;
//        List<Transaction> transactionList = transactionRepo.findAll();
//        for (int i = 0; i < transactionList.size(); i++) {
//            String[] transactionDate = transactionList.get(i).getCreated_date().toString().split(" ");
//            LocalDate localDate = LocalDate.parse(transactionDate[0]);
//            if (localDate.isAfter(lcdate) && localDate.isBefore(lcdate_new) || localDate.isEqual(lcdate_new) || localDate.isEqual(lcdate)) {
//                totalPrice += Integer.parseInt(transactionList.get(i).getTotalPrice());
//            }
//        }
//        check = true;
//        model.addAttribute("date", lcdate_new);
//        model.addAttribute("datenew", lcdate_new);
//       model.addAttribute("totalPrice", decimalFormat.format( totalPrice / 1000000.0));
//        response.sendRedirect("/profits");
//    }

}
