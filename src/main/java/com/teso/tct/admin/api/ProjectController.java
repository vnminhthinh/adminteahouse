package com.teso.tct.admin.api;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.teso.framework.utils.ConvertUtils;
import com.teso.framework.utils.JSONUtil;
import com.teso.framework.utils.NetworkUtils;
import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.MenuRepo;
import com.teso.tct.admin.db.ProjectDB;
import com.teso.tct.admin.ent.Menu;
import com.teso.tct.admin.ent.Project;
import com.teso.tct.admin.service.PageService;
import com.teso.tct.admin.service.PageServiceProject;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.utils.ApiAuthenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

@Controller
public class ProjectController {
    @Autowired
    private ProjectDB projectDB;
    @Autowired
    PageService pageService;
    @Autowired
    private MenuRepo menuRepo;
    @Autowired
    PageServiceProject pageServiceProject;

    private final StorageService storageService;
    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private final String UPLOAD_FOLDER = ConfigInfo.UPLOAD_FOLDER;
    private boolean DESCRIPTION = ConvertUtils.toBoolean(ConfigInfo.DESCRIPTION_PROJECT);
    private boolean IMAGE = ConvertUtils.toBoolean(ConfigInfo.IMAGE);
    private boolean MENU = ConvertUtils.toBoolean(ConfigInfo.MENU);
    private String apiUrl = ConfigInfo.API_URL;
    private String thumbnail = "";

    public ProjectController(StorageService storageService) {
        this.storageService = storageService;
    }

    @RequestMapping("/projects")
    public String items(Model model, @RequestParam(name = "page", defaultValue = "1") int page) {
        model.addAttribute("page_content", "project.ftl");
        model.addAttribute("upload_url", UPLOAD_URL);
        model.addAttribute("image", IMAGE);
        model.addAttribute("description", DESCRIPTION);
        String responseProject = NetworkUtils.getResponse(apiUrl + "project-panging/" + page);
        Type list = new TypeToken<List<Project>>() {
        }.getType();
        JsonObject arr = JSONUtil.DeSerialize(responseProject, JsonObject.class);
        List<Project> projectList = JSONUtil.DeSerialize(arr.get("items").toString(), list);
        int k = ConvertUtils.toInt(arr.get("totalPages").toString());
        model.addAttribute("projectList", projectList);
        model.addAttribute("page", k);
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("prePage", page - 1);
        model.addAttribute("currentPage", page);
        model.addAttribute("menu", Boolean.parseBoolean(ConfigInfo.MENU));
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/add_project")
    public String addProject(Model model, HttpServletRequest request,
                             HttpServletResponse response) {
        model.addAttribute("page_content", "add_project.ftl");
        model.addAttribute("image", IMAGE);
        String responseMenu = NetworkUtils.getResponse(apiUrl + "menu-all");
        Type listmenu = new TypeToken<List<Menu>>() {
        }.getType();
        List<Menu> listMenu = JSONUtil.DeSerialize(responseMenu, listmenu);
        model.addAttribute("menuList", listMenu);
        model.addAttribute("menu", MENU);
        model.addAttribute("description", DESCRIPTION);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("createProject")
    public void createProject(Model model, @RequestParam("thumbnail") MultipartFile files, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        JsonObject objectProject = new JsonObject();
        objectProject.addProperty("name", name);
        if (DESCRIPTION) {
            objectProject.addProperty("description", description);
        } else {
            objectProject.addProperty("description", "");
        }
        objectProject.addProperty("type", type);
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            objectProject.addProperty("thumbnail", UPLOAD_FOLDER + files.getOriginalFilename());
        } else {
            objectProject.addProperty("thumbnail", UPLOAD_FOLDER + "abc.jpg");
        }
        if (MENU) {
            String menu_id = request.getParameter("menu_id");
            String responseMenu = NetworkUtils.getResponse(apiUrl + "menu-byid/" + menu_id);
            JsonObject object = JSONUtil.DeSerialize(responseMenu, JsonObject.class);
            Menu menu = new Menu();
            menu.setId(object.get("id").getAsString());
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", object.get("id").getAsString());
            objectProject.add("menu", jsonObject);
        }


        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        String result = NetworkUtils.doPost(apiUrl + "project-add", header, objectProject);
        System.out.println("result" + result);
        response.sendRedirect("/add_project");
    }

    @PostMapping("createProjectMenu")
    public void createProjectMenu(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        JsonObject objectProject = new JsonObject();
        objectProject.addProperty("name", name);
        if (DESCRIPTION) {
            objectProject.addProperty("description", description);
        } else {
            objectProject.addProperty("description", "");
        }
        objectProject.addProperty("type", type);
        objectProject.addProperty("thumbnail", "");
        if (MENU) {
            String menu_id = request.getParameter("menu_id");
            String responseMenu = NetworkUtils.getResponse(apiUrl + "menu-byid/" + menu_id);
            JsonObject object = JSONUtil.DeSerialize(responseMenu, JsonObject.class);
            Menu menu = new Menu();
            menu.setId(object.get("id").getAsString());
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("id", object.get("id").getAsString());
            objectProject.add("menu", jsonObject);
        }

        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        String result = NetworkUtils.doPost(apiUrl + "project-add", header, objectProject);
        System.out.println("result" + result);
        response.sendRedirect("/add_project");
    }

    @RequestMapping("/removeProject")
    public void removeProject(HttpServletResponse response, @RequestParam(name = "id", defaultValue = "0") String id)
            throws IOException {
        NetworkUtils.getResponse(apiUrl + "project-delete/" + id);
        response.sendRedirect("/projects");
    }

    @RequestMapping("/update")
    public String updateProject(Model model, @RequestParam(name = "id", defaultValue = "0") String id) {
        model.addAttribute("page_content", "update_project.ftl");
        model.addAttribute("image", IMAGE);
        model.addAttribute("menu", MENU);
        model.addAttribute("description", DESCRIPTION);
        String response = NetworkUtils.getResponse(apiUrl + "project-byid/" + id);
        JsonObject object = JSONUtil.DeSerialize(response, JsonObject.class);
        List<Project> projectList = new ArrayList<>();
        Project project = new Project();
        project.setName(object.get("name").getAsString());
        if (IMAGE) {
            project.setThumbnail(object.get("thumbnail").getAsString());
            thumbnail = object.get("thumbnail").getAsString();
        }
        if (DESCRIPTION) {
            project.setDescription(object.get("description").getAsString());
        }
        project.setType(object.get("type").getAsString());
        project.setId(object.get("id").getAsString());
        if (MENU) {
            JsonObject objectMenu = object.getAsJsonObject("menu");
            Menu menu = new Menu();
            menu.setId(objectMenu.get("id").getAsString());
            menu.setType(objectMenu.get("type").getAsString());
            menu.setThumbnail(objectMenu.get("thumbnail").getAsString());
            menu.setName(object.get("name").getAsString());
            menu.setDescription(object.get("description").getAsString());
            project.setMenu(menu);
        }
        projectList.add(project);

        model.addAttribute("projects", projectList);
        String responseMenu = NetworkUtils.getResponse(apiUrl + "/menu-all");
        Type listmenu = new TypeToken<List<Menu>>() {
        }.getType();
        List<Menu> listMenu = JSONUtil.DeSerialize(responseMenu, listmenu);
        model.addAttribute("menuList", listMenu);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("/updateProject")
    public void updateProject(@RequestParam(name = "id", defaultValue = "0") String id,
                              HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String menu_id = request.getParameter("menu_id");
        JsonObject object = new JsonObject();
        if (name != null) {
            object.addProperty("name", name);
        } else {
            object.addProperty("name", "");
        }
        if (DESCRIPTION) {
            if (description != null) {
                object.addProperty("description", description);
            } else {
                object.addProperty("description", "");
            }
        }
        if (type != null) {
            object.addProperty("type", type);
        } else {
            object.addProperty("type", "");
        }
        object.addProperty("thumbnail", "");
        if (MENU) {
            String responseMenu = NetworkUtils.getResponse(apiUrl + "menu-byid/" + menu_id);
            JsonObject objectMenu = JSONUtil.DeSerialize(responseMenu, JsonObject.class);
            object.add("menu", objectMenu);
        }
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        NetworkUtils.doPost(apiUrl + "project-update/" + id, header, object);
        response.sendRedirect("/projects");
    }

    @PostMapping("/updateProjectImage")
    public void updateProjectImage(@RequestParam(name = "id", defaultValue = "0") String id,
                                   HttpServletRequest request, HttpServletResponse response, @RequestParam("thumbnail") MultipartFile files) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String menu_id = request.getParameter("menu_id");
        JsonObject object = new JsonObject();
        if (name != null) {
            object.addProperty("name", name);
        } else {
            object.addProperty("name", "");
        }
        if (description != null) {
            object.addProperty("description", description);
        } else {
            object.addProperty("description", "");
        }
        if (type != null) {
            object.addProperty("type", type);
        } else {
            object.addProperty("type", "");
        }
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            object.addProperty("thumbnail", UPLOAD_FOLDER + files.getOriginalFilename());
        } else {
            object.addProperty("thumbnail", thumbnail);
        }
        if (MENU) {
            String responseMenu = NetworkUtils.getResponse(apiUrl + "menu-byid/" + menu_id);
            JsonObject objectMenu = JSONUtil.DeSerialize(responseMenu, JsonObject.class);
            object.add("menu", objectMenu);
        }
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        NetworkUtils.doPost(apiUrl + "project-update/" + id, header, object);
        response.sendRedirect("/projects");
    }
}
