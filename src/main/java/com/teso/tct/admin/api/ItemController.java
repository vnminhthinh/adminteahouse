package com.teso.tct.admin.api;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.teso.framework.utils.ConvertUtils;
import com.teso.framework.utils.JSONUtil;
import com.teso.framework.utils.NetworkUtils;
import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.*;
import com.teso.tct.admin.ent.*;
import com.teso.tct.admin.model.*;
import com.teso.tct.admin.service.PageService;
import com.teso.tct.admin.service.PageServiceProject;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.utils.ApiAuthenUtils;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.tools.asm.Cover;

@Controller
public class ItemController {

    private final StorageService storageService;
    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private final String UPLOAD_FOLDER = ConfigInfo.UPLOAD_FOLDER;
    private boolean PRICE = ConvertUtils.toBoolean(ConfigInfo.PRICE);
    private String apiUrl = ConfigInfo.API_URL;
    private String imgUrl = "";
    private boolean MENU = ConvertUtils.toBoolean(ConfigInfo.MENU);
    private boolean DESCRIPTION_PROJECT = ConvertUtils.toBoolean(ConfigInfo.DESCRIPTION_PROJECT);
    private boolean DESCRIPTION = ConvertUtils.toBoolean(ConfigInfo.DESCRIPTION_ITEM);
    private boolean TITLE = ConvertUtils.toBoolean(ConfigInfo.TITLE_ITEM);
    private boolean CONTENT = ConvertUtils.toBoolean(ConfigInfo.CONTENT_ITEM);

    @Autowired
    public ItemController(StorageService storageService) {
        this.storageService = storageService;
    }


    @RequestMapping("/items")
    public String orderItems(Model model, @RequestParam(name = "page", defaultValue = "1") int page) {
        model.addAttribute("page_content", "items.ftl");
        model.addAttribute("des", DESCRIPTION);
        model.addAttribute("title", TITLE);
        model.addAttribute("content", CONTENT);
        String responseItem = NetworkUtils.getResponse(apiUrl + "item-panging/" + page);
        JsonObject jsonObject = JSONUtil.DeSerialize(responseItem, JsonObject.class);
        List<Item> itemList = new ArrayList<>();
        JsonArray jsonArray = jsonObject.getAsJsonArray("items");
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonobject = jsonArray.get(i).getAsJsonObject();
            Item item = new Item();
            item.setId(jsonobject.get("id").getAsString());
            if (CONTENT) {
                item.setContent(jsonobject.get("content").getAsString());
            }
            if (TITLE) {
                item.setTitle(jsonobject.get("title").getAsString());
            }
            item.setImgUrl(jsonobject.get("imgUrl").getAsString());
            if (PRICE) {
                item.setPrice(jsonobject.get("price").getAsString());
            }
            if (DESCRIPTION) {
                item.setDescription(jsonobject.get("description").getAsString());
            }
            JsonObject jsonProject = jsonobject.get("project").getAsJsonObject();
            Project project = new Project();
            project.setId(jsonProject.get("id").getAsString());
            project.setType(jsonProject.get("type").getAsString());
            if (DESCRIPTION_PROJECT) {
                project.setDescription(jsonProject.get("description").getAsString());
            }
            project.setName(jsonProject.get("name").getAsString());
            project.setThumbnail(jsonProject.get("thumbnail").getAsString());
            item.setProject(project);
            itemList.add(item);
        }
        int k = ConvertUtils.toInt(jsonObject.get("totalPages").toString());
        model.addAttribute("menu", MENU);
        model.addAttribute("price", PRICE);
        model.addAttribute("itemList", itemList);
        model.addAttribute("upload_url", UPLOAD_URL);
        model.addAttribute("page", k);
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("prePage", page - 1);
        model.addAttribute("currentPage", page);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/add_item")
    public String addItem(Model model, HttpServletRequest request,
                          HttpServletResponse response) {
        model.addAttribute("page_content", "add_items.ftl");
        model.addAttribute("des", DESCRIPTION);
        model.addAttribute("title", TITLE);
        model.addAttribute("content", CONTENT);
        String responseProject = NetworkUtils.getResponse(apiUrl + "project-dropdownlist");
        Type listProject = new TypeToken<List<Project>>() {
        }.getType();
        List<Project> projectList = JSONUtil.DeSerialize(responseProject, listProject);
        model.addAttribute("projects", projectList);
        model.addAttribute("price", PRICE);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }


    @PostMapping("/createItem")
    public void createItem(Model model, @RequestParam("imgUrl") MultipartFile files, HttpServletRequest request,
                           HttpServletResponse response) throws IOException, ParseException {
        String content = request.getParameter("content");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String price = request.getParameter("price");
        String project_id = request.getParameter("project_id");
        String showHome = request.getParameter("showHome");
        JsonObject objectProject = new JsonObject();
        if (TITLE) {
            objectProject.addProperty("title", title);
        }
        if (DESCRIPTION) {
            objectProject.addProperty("description", description);
        }
        if (CONTENT) {
            objectProject.addProperty("content", content);
        }
        if (showHome != null) {
            objectProject.addProperty("isShowHomePage", ConvertUtils.toBoolean(showHome));
        } else {
            objectProject.addProperty("isShowHomePage", false);
        }
        objectProject.addProperty("price", price);
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            objectProject.addProperty("imgUrl", UPLOAD_FOLDER + files.getOriginalFilename());
        } else {
            objectProject.addProperty("imgUrl", UPLOAD_FOLDER + "abc.jpg");
        }
        String responseProject = NetworkUtils.getResponse(apiUrl + "project-byid/" + project_id);
        JsonObject object = JSONUtil.DeSerialize(responseProject, JsonObject.class);
        Project project = new Project();
        project.setId(object.get("id").getAsString());
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", object.get("id").getAsString());
        objectProject.add("project", jsonObject);
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        NetworkUtils.doPost(apiUrl + "item-add", header, objectProject);
        response.sendRedirect("/add_item");
    }


    @RequestMapping("/removeItem")
    public void removeItem(HttpServletResponse response, @RequestParam(name = "id", defaultValue = "0") String id)
            throws IOException {
        NetworkUtils.getResponse(apiUrl + "item-delete/" + id);
        response.sendRedirect("/items");
    }

    @RequestMapping("/item-content-detail")
    public String portlolioDetailCatagoly(Model model, @RequestParam(name = "id", defaultValue = "0") String id) {
        String response = NetworkUtils.getResponse(apiUrl + "item-byid/" + id);
        JsonObject object = JSONUtil.DeSerialize(response, JsonObject.class);
        List<Item> items = new ArrayList<>();
        Item item = new Item();
        item.setContent(object.get("content").getAsString());
        items.add(item);
        model.addAttribute("listItems", items);
        return "content-detail";
    }

    @RequestMapping("/updateItem")
    public String updateItem(Model model, @RequestParam(name = "id", defaultValue = "0") String id) {
        model.addAttribute("page_content", "update_item.ftl");
        model.addAttribute("des", DESCRIPTION);
        model.addAttribute("title", TITLE);
        model.addAttribute("content", CONTENT);
        String responseProject = NetworkUtils.getResponse(apiUrl + "project-dropdownlist");
        Type listProject = new TypeToken<List<Project>>() {
        }.getType();
        List<Project> projectList = JSONUtil.DeSerialize(responseProject, listProject);
        String response = NetworkUtils.getResponse(apiUrl + "item-byid/" + id);
        JsonObject jsonobject = JSONUtil.DeSerialize(response, JsonObject.class);

        model.addAttribute("projects", projectList);
        List<Item> itemList = new ArrayList<>();
        Item item = new Item();
        item.setId(jsonobject.get("id").getAsString());
        if (CONTENT) {
            item.setContent(jsonobject.get("content").getAsString());
        }
        if (TITLE) {
            item.setTitle(jsonobject.get("title").getAsString());
        }
        item.setImgUrl(jsonobject.get("imgUrl").getAsString());
        imgUrl = jsonobject.get("imgUrl").getAsString();
        if (PRICE) {
            item.setPrice(jsonobject.get("price").getAsString());
        }
        if (DESCRIPTION) {
            item.setDescription(jsonobject.get("description").getAsString());
        }
        JsonObject jsonProject = jsonobject.get("project").getAsJsonObject();
        Project project = new Project();
        project.setId(jsonProject.get("id").getAsString());
        project.setType(jsonProject.get("type").getAsString());
        if (DESCRIPTION_PROJECT) {
            project.setDescription(jsonProject.get("description").getAsString());
        }
        project.setName(jsonProject.get("name").getAsString());
        project.setThumbnail(jsonProject.get("thumbnail").getAsString());
        item.setProject(project);
        itemList.add(item);
        model.addAttribute("listItems", itemList);
        model.addAttribute("price", PRICE);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("/updateItem")
    public void updateItem(@RequestParam("imgUrl") MultipartFile files, @RequestParam(name = "id", defaultValue = "0") String id,
                           HttpServletRequest request, HttpServletResponse response) throws IOException {
        String content = request.getParameter("content");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String project_id = request.getParameter("project_id");
        String price = request.getParameter("price");
        JsonObject object = new JsonObject();
        if (CONTENT) {
            if (!content.isEmpty()) {
                object.addProperty("content", content);
            }
        }
        if (TITLE) {
            if (!title.isEmpty()) {
                object.addProperty("title", title);
            }
        }
        if (DESCRIPTION) {
            if (!description.isEmpty()) {
                object.addProperty("description", description);
            }
        }
        if (PRICE) {
            if (!price.isEmpty()) {
                object.addProperty("price", price);
            }
        }
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            object.addProperty("imgUrl", UPLOAD_FOLDER + files.getOriginalFilename());
        } else {
            object.addProperty("imgUrl", imgUrl);
        }
        String responseProject = NetworkUtils.getResponse(apiUrl + "project-byid/" + project_id);
        JsonObject objectProject = JSONUtil.DeSerialize(responseProject, JsonObject.class);
        object.add("project", objectProject);
        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        NetworkUtils.doPost(apiUrl + "item-update/" + id, header, object);
        response.sendRedirect("/items");
    }

}
