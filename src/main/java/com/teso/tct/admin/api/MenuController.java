package com.teso.tct.admin.api;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.teso.framework.utils.ConvertUtils;
import com.teso.framework.utils.JSONUtil;
import com.teso.framework.utils.NetworkUtils;
import com.teso.framework.utils.StringUtils;
import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.MenuRepo;
import com.teso.tct.admin.ent.Menu;
import com.teso.tct.admin.ent.Project;
import com.teso.tct.admin.model.MMenu;
import com.teso.tct.admin.service.PageService;
import com.teso.tct.admin.service.PageServiceProject;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.utils.ApiAuthenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

@Controller
public class MenuController {
    @Autowired
    private MenuRepo menuRepo;
    @Autowired
    PageService pageService;

    @Autowired
    PageServiceProject pageServiceProject;
    private final StorageService storageService;
    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private final String UPLOAD_FOLDER = ConfigInfo.UPLOAD_FOLDER;
    private int PAGE_SIZE = Integer.parseInt(ConfigInfo.PAGE_SIZE);
    private boolean IMAGE = Boolean.parseBoolean(ConfigInfo.IMAGE);
    private String apiUrl = ConfigInfo.API_URL;
    private String thumbnail = "";

    public MenuController(StorageService storageService) {
        this.storageService = storageService;
    }

    @RequestMapping("/menus")
    private String menus(Model model, @RequestParam(name = "page", defaultValue = "1") int page) {
        model.addAttribute("page_content", "menus.ftl");
        model.addAttribute("upload_url", UPLOAD_URL);
        String responseMenu = NetworkUtils.getResponse(apiUrl + "menu-panging/" + page);
        Type listmenu = new TypeToken<List<Menu>>() {
        }.getType();
        JsonObject arr = JSONUtil.DeSerialize(responseMenu, JsonObject.class);
        List<Menu> listMenu = JSONUtil.DeSerialize(arr.get("items").toString(), listmenu);
        int k = ConvertUtils.toInt(arr.get("totalPages").toString());
        model.addAttribute("menuList", listMenu);
        model.addAttribute("page", k);
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("prePage", page - 1);
        model.addAttribute("currentPage", page);
        model.addAttribute("image", IMAGE);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/add_menu")
    public String addMenu(Model model) {
        model.addAttribute("page_content", "add_menu.ftl");
        model.addAttribute("image", IMAGE);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");

    }

    @PostMapping("createMenu")
    public void createMenu(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        JsonObject object = new JsonObject();
        if (name != null) {
            object.addProperty("name", name);
        } else {
            object.addProperty("name", "");
        }
        if (description != null) {
            object.addProperty("description", description);
        } else {
            object.addProperty("description", "");
        }
        if (type != null) {
            object.addProperty("type", type);
        } else {
            object.addProperty("type", "");
        }

        object.addProperty("thumbnail", "");


        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        String result = NetworkUtils.doPost(apiUrl + "menu-add", header, object);
        System.out.println("result" + result);
        response.sendRedirect("/add_menu");
    }

    @PostMapping("createMenuThumbnail")
    public void createMenuThumbnail(HttpServletRequest request, HttpServletResponse response, @RequestParam("thumbnail") MultipartFile files) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        JsonObject object = new JsonObject();
        if (name != null) {
            object.addProperty("name", name);
        } else {
            object.addProperty("name", "");
        }
        if (description != null) {
            object.addProperty("description", description);
        } else {
            object.addProperty("description", "");
        }
        if (type != null) {
            object.addProperty("type", type);
        } else {
            object.addProperty("type", "");
        }
        if (IMAGE) {
            if (!files.getOriginalFilename().isEmpty()) {
                storageService.store(files);
                object.addProperty("thumbnail", UPLOAD_FOLDER + files.getOriginalFilename());
            } else {
                object.addProperty("thumbnail", UPLOAD_FOLDER + "abc.jpg");
            }
        } else {
            object.addProperty("thumbnail", "");
        }


        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        NetworkUtils.doPost("http://localhost:9001/api/menu-add", header, object);
        response.sendRedirect("/add_menu");
    }

    @RequestMapping("/removeMenu")
    public void removeMenu(HttpServletResponse response, @RequestParam(name = "id", defaultValue = "0") String id)
            throws IOException {
        NetworkUtils.getResponse(apiUrl + "menu-delete/" + id);
        response.sendRedirect("/menus");
    }

    @RequestMapping("/updateMenu")
    public String updateMenu(Model model, @RequestParam(name = "id", defaultValue = "0") String id) {
        model.addAttribute("page_content", "update_menu.ftl");
        model.addAttribute("image", IMAGE);
        String responseMenu = NetworkUtils.getResponse(apiUrl + "menu-byid/" + id);
        JsonObject object = JSONUtil.DeSerialize(responseMenu, JsonObject.class);
        List<Menu> listMenu = new ArrayList<>();
        Menu menu = new Menu();
        menu.setId(object.get("id").getAsString());
        menu.setName(object.get("name").getAsString());
        menu.setDescription(object.get("description").getAsString());
        menu.setThumbnail(object.get("thumbnail").getAsString());
        menu.setType(object.get("type").getAsString());
        listMenu.add(menu);
        thumbnail = object.get("thumbnail").getAsString();
        model.addAttribute("menuList", listMenu);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("/updateMenu")
    public void updateMenu(@RequestParam(name = "id", defaultValue = "0") String id,
                           HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        JsonObject object = new JsonObject();
        if (name != null) {
            object.addProperty("name", name);
        } else {
            object.addProperty("name", "");
        }
        if (description != null) {
            object.addProperty("description", description);
        } else {
            object.addProperty("description", "");
        }
        if (type != null) {
            object.addProperty("type", type);
        } else {
            object.addProperty("type", "");
        }

        object.addProperty("thumbnail", "");


        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        String result = NetworkUtils.doPost(apiUrl + "menu-update/" + id, header, object);
        response.sendRedirect("/menus");
    }

    @PostMapping("/updateMenuImage")
    public void updateMenuImage(@RequestParam(name = "id", defaultValue = "0") String id,
                                HttpServletRequest request, HttpServletResponse response, @RequestParam("thumbnail") MultipartFile files) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        JsonObject object = new JsonObject();
        if (name != null) {
            object.addProperty("name", name);
        } else {
            object.addProperty("name", "");
        }
        if (description != null) {
            object.addProperty("description", description);
        } else {
            object.addProperty("description", "");
        }
        if (type != null) {
            object.addProperty("type", type);
        } else {
            object.addProperty("type", "");
        }
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            object.addProperty("thumbnail", UPLOAD_FOLDER + files.getOriginalFilename());
        } else {
            object.addProperty("thumbnail", thumbnail);
        }

        Map<String, String> header = new HashMap<>();
        header.put("Content-Type", "application/json");
        NetworkUtils.doPost(apiUrl + "menu-update/" + id, header, object);
        response.sendRedirect("/menus");
    }
}
