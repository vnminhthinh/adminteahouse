<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
  <meta charset="utf-8"/>
  <title>Login Page - Ace Admin</title>

  <meta name="description" content="User login page"/>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>

  <link rel="stylesheet" href="assets/css/loading.css"/>

  <!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css"/>

  <!-- text fonts -->
  <link rel="stylesheet" href="assets/css/fonts.googleapis.com.css"/>

  <!-- ace styles -->
  <link rel="stylesheet" href="assets/css/ace.min.css"/>

  <!--[if lte IE 9]>
  <link rel="stylesheet" href="assets/css/ace-part2.min.css"/>
  <![endif]-->
  <link rel="stylesheet" href="assets/css/ace-rtl.min.css"/>

  <!--[if lte IE 9]>
  <link rel="stylesheet" href="assets/css/ace-ie.min.css"/>
  <![endif]-->

  <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

  <!--[if lte IE 8]>
  <script src="assets/js/html5shiv.min.js"></script>
  <script src="assets/js/respond.min.js"></script>
  <![endif]-->
  <!-- Google api-->
  <script src="https://apis.google.com/js/api:client.js"></script>
  <!--------------->
  <script>
    var googleUser = {};

    var startApp = function () {
      gapi.load('auth2', function () {
        auth2 = gapi.auth2.init({
          client_id: '${client_id}',
          cookiepolicy: 'single_host_origin',
        });
        attachSignIn(document.getElementById('idGooglePlus'));
        $("#idLoading").hide();
        $("#idLoader").hide();
        $("#idGooglePlus").on("click", function () {
          $("#idLoading").show();
        })
      });
    };

    function attachSignIn(element) {
      auth2.attachClickHandler(element, {},
          function (googleUser) {
            // $("#idLoading").show();
            $("#idLoader").show();
            let userData = {
              open_id: googleUser.getBasicProfile().getId(),
              open_name: googleUser.getBasicProfile().getName(),
              open_email: googleUser.getBasicProfile().getEmail(),
              open_avatar: googleUser.getBasicProfile().getImageUrl()
            }
            // $.get("/login", userData,
            //     function (data, status) {
            //       alert("Data: " + data + "\nStatus: " + status);
            //     }
            // );
            $("input[name='username']").val(JSON.stringify(userData));
            $("form").submit();
          });
    }
  </script>
</head>

<body class="login-layout light-login">
<div class="main-container">
  <div class="main-content">
    <div class="row">
      <div class="col-sm-10 col-sm-offset-1">
        <div class="login-container">
          <div class="center">
            <h1>
              <i class="ace-icon fa fa-leaf green"></i>
              <span class="red">Teso</span>
              <span class="grey" id="id-text2">Application</span>
            </h1>
            <h4 class="blue" id="id-company-text">&copy; ${company_name}</h4>
          </div>

          <div class="space-6"></div>

          <div class="position-relative">
            <div id="login-box" class="login-box visible widget-box no-border">
              <div class="widget-body">
                <div class="widget-main">
                  <h4 class="header blue lighter bigger" style="text-align: center">
                    <i class="ace-icon fa fa-coffee green"></i>
                    Please login by Google
                  </h4>
                  <div class="space-6"></div>
                  <form action="/login" method="post">
                    <input type="hidden" name="username">
                    <input type="hidden" name="password" value="1">
                    <div class="social-login center">
                      <a id="idGooglePlus" class="btn btn-danger">
                        <i class="ace-icon fa fa-google-plus"></i>
                      </a>
                    </div>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                  </form>
                  <div class="space-6"></div>
                    <div>
                    </div>
                </div><!-- /.widget-main -->
              </div><!-- /.widget-body -->
            </div><!-- /.login-box -->
          </div><!-- /.position-relative -->
        </div>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.main-content -->
  <div id="idLoading">
    <div class='loading_container'>
    </div>
    <div id="idLoader" class='loader'>
      <div class='loader--dot'></div>
      <div class='loader--dot'></div>
      <div class='loader--dot'></div>
      <div class='loader--dot'></div>
      <div class='loader--dot'></div>
      <div class='loader--dot'></div>
      <div class='loader--text'></div>
    </div>
  </div>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="assets/js/jquery-2.1.4.min.js"></script>
<script>startApp();</script>
<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
  if ('ontouchstart' in document.documentElement) {
    document.write(
        "<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
  }
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
  jQuery(function ($) {
    $(document).on('click', '.toolbar a[data-target]', function (e) {
      e.preventDefault();
      var target = $(this).data('target');
      $('.widget-box.visible').removeClass('visible');//hide others
      $(target).addClass('visible');//show target
    });
  });
</script>
</body>
</html>
