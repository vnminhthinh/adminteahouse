<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.js"
            data-modules="effect effect-bounce effect-blind effect-bounce effect-clip effect-drop effect-fold effect-slide"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>


<style type="text/css">
    .expireItem {
        color: red;
    }
</style>

<div class="page-content" style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
<#--<div class="page-header">-->
<#--<h1>-->
<#--Manage Item-->
<#--<small>-->
<#--<i class="ace-icon fa fa-angle-double-right"></i>-->
<#--Item List-->
<#--</small>-->
<#--</h1>-->
<#--</div>-->
    <!-- /.page-header -->
    <div class="row">

        <div class="col-xs-12">
            <div style="font-family: 'Arial Unicode MS';">
                <div class="row">

                    <div class="col-xs-12">
                        <form method="post">
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                            Chọn ngày: <input style="color: red" type="text" class="startdate" name="date"
                                              value="${dateOld}"/>
                            đến ngày: <input style="color: red" type="text" name="datenew" class="enddate"
                                             value="${dateNow}">
                            <input class="btn btn-info" type="submit" value="Xem" formaction="/profits">
                            <br><br>
                            <input class="btn btn-info" type="submit" value="Hôm nay"
                                   formaction="/profits_now">
                            <input class="btn btn-info" type="submit" value="Hôm qua"
                                   formaction="/profits_yesterday">
                            <input class="btn btn-info" type="submit" value="7 Ngày trước"
                                   formaction="/profits_week">
                            <input class="btn btn-info" type="submit" value="30 Ngày trước"
                                   formaction="/profits_month">
                        <#--<input class="btn btn-info" type="submit" value="6 tháng trước" formaction="/profits_six_month">-->
                        <#--<input class="btn btn-info" type="submit" value="1 năm trước" formaction="/profits_year">-->
                            <div class="space-10"></div>
                        </form>
                        <div style="width:100%;">
                            <canvas id="canvas"></canvas>
                        </div>
                        <div class="space-10"></div>
                        <div class="space-10"></div>
                        <div style="width:70%;margin-left: 200px">
                            <canvas id="chart-area"></canvas>
                        </div>
                        <div class="space-10"></div>
                        <p style="margin-left: 470px">Thống kế doanh thu cho từ loại sản phẩm </p>
                        <div class="space-10"></div>
                        <div class="space-10"></div>

                        <div class="table-header"
                             style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
                            Bảng thống kê doanh thu

                        </div>
                        <div>
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th style="text-align: center;">Ngày thống kê</th>
                                    <th style="text-align: center;">Tổng tiền (Triệu VNĐ)</th>
                                </tr>
                                </thead>

                                <tbody style="font-family: 'Arial Unicode MS'; text-transform: none;">
                                            <#list list as list>
                                                <tr>

                                                    <td style="text-align: center">Ngày: ${list.date} </td>
                                                    <#if list.totalPrice =='.00'>
                                                    <td style="text-align: center">0</td>
                                                    <#else >
                                                    <td style="text-align: center">${list.totalPrice}</td>
                                                    </#if>
                                                </tr>
                                            </#list>

                                <td style="text-align: center">Tổng Tiền</td>
                                            <#if totalPrice =='.00'>
                                            <td style="text-align: center">0</td>
                                            <#else >
                                              <td style="text-align: center">${totalPrice}</td>
                                            </#if>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- PAGE CONTENT ENDS -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>


<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<!-- page specific plugin scripts -->
<script src="assets/js/jquery.bootstrap-duallistbox.min.js"></script>
<script src="assets/js/jquery.raty.min.js"></script>
<script src="assets/js/bootstrap-multiselect.min.js"></script>
<script src="assets/js/select2.min.js"></script>
<script src="assets/js/jquery-typeahead.js"></script>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->
		<script src="assets/js/jquery-ui.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/chosen.jquery.min.js"></script>
		<script src="assets/js/spinbox.min.js"></script>

		<script src="assets/js/moment.min.js"></script>
		<script src="assets/js/daterangepicker.min.js"></script>
		<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
		<script src="assets/js/bootstrap-colorpicker.min.js"></script>
		<script src="assets/js/jquery.knob.min.js"></script>
		<script src="assets/js/autosize.min.js"></script>
		<script src="assets/js/jquery.inputlimiter.min.js"></script>
		<script src="assets/js/jquery.maskedinput.min.js"></script>
		<script src="assets/js/bootstrap-tag.min.js"></script>

<!-- ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>
<script src="assets/js/Chart.bundle.js"></script>
	<script src="assets/js/utils.js"></script>
	<style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
<script>
    var config = {
        type: 'line',

        data: {
            labels: [],
            datasets: [{
                label: 'Doanh Thu (Triệu VNĐ)',
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.red,

                data: [],
                fill: false,
            }]
        },

        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Doanh Thu ${date} - ${datenew}'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Ngày'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Triệu VNĐ'
                    },
                    ticks:{
                        min:0
                    }
                }]
            }
        }
    };
    var config1 = {
        type: 'pie',
        data: {
            datasets: [{
                data: [],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.orange,
                    window.chartColors.yellow,
                    window.chartColors.green,
                    window.chartColors.blue,
                    window.chartColors.purple,
                    window.chartColors.teal,
                    window.chartColors.olive,
                    window.chartColors.grey,
                    window.chartColors.navy,
                ],
                label: 'Dataset 1'
            }],
            labels: []
        },
        options: {
            responsive: true
        }
    };
    window.onload = function () {
        // line chart
        var ctx = document.getElementById('canvas').getContext('2d');
        window.myLine = new Chart(ctx, config);
        <#list list as list>
            var month = "${list.date}";
            config.data.labels.push(month);
            var i = ${list.totalPrice};
            config.data.datasets.forEach(function (dataset) {
                dataset.data.push(i);
            });
        </#list>
        window.myLine.update();

        // pie chart
        var ctx1 = document.getElementById('chart-area').getContext('2d');
        window.myPie = new Chart(ctx1, config1);
        <#list listPie as list>
            var a = "${list.item}"
            config1.data.labels.push(a);
            var y = ${list.totalPrice};
            config1.data.datasets.forEach(function (dataset) {
                dataset.data.push(y)
            });
        </#list>

        window.myPie.update();

    };
</script>
<script>
    $(function () {
        $(".startdate").datepicker({
            dateFormat: 'dd/mm/yy',//check change
            changeMonth: true,
            changeYear: true
        });
    })
    $(function () {
        $(".enddate").datepicker({
            dateFormat: 'dd/mm/yy',//check change
            changeMonth: true,
            changeYear: true
        });
    })
</script>