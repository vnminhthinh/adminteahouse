<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>
    <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
            try {
                ace.settings.loadState('sidebar')
            } catch (e) {
            }
        </script>
        <ul class="nav nav-list" style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
      <#if page_1 == true>
        <li class="active open">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-list-alt"></i>
                <span class="menu-text"> Quản Trị </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>

            <ul class="submenu">
                <li class="<#if active_admin_info??>active</#if>">
                    <a href="/adminInfo">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Danh Sách Quản Trị Viên
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="<#if active_admin_perm??>active</#if>">
                    <a href="/adminPermission">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Thêm Quyền
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="<#if active_admin_user??>active</#if>">
                    <a href="/adminUser">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Thêm Người Quản Trị
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
      </#if>
        <#if menu == true>
        <li class="active open">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-tag"></i>
                <span class="menu-text">Quản Lý Menu </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li class="<#if active_admin_info??>active</#if>">
                    <a href="/menus">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Danh Sách Menu
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="<#if active_admin_perm??>active</#if>">
                    <a href="/add_menu">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Thêm Menu
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
        </#if>
      <#if page_2 == true>
        <li class="active open">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-tag"></i>
                <span class="menu-text">Quản Lý Danh Mục </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li class="<#if active_admin_info??>active</#if>">
                    <a href="/projects">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Danh Sách Danh Mục
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="<#if active_admin_perm??>active</#if>">
                    <a href="/add_project">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Thêm Danh Mục
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
      </#if>
        <#if menu ==true>
            <li class="active open">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-tag"></i>
                    <span class="menu-text"> Quản Lý Bài Viết </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="<#if active_admin_perm??>active</#if>">
                        <a href="/items">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Danh Sách Bài Viết
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="<#if active_admin_perm??>active</#if>">
                        <a href="/add_item">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Thêm Bài Viết
                        </a>
                        <b class="arrow"></b>
                    </li>

                </ul>
            </li>

        <#else >
                <li class="active open">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-tag"></i>
                        <span class="menu-text"> Quản Lý Sản Phẩm </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="<#if active_admin_perm??>active</#if>">
                            <a href="/items">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Danh Sách Sản Phẩm
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<#if active_admin_perm??>active</#if>">
                            <a href="/add_item">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Thêm Sản Phẩm
                            </a>
                            <b class="arrow"></b>
                        </li>

                    </ul>
                </li>
        </#if>
        <#if transaction == true>
            <li class="active open">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-tag"></i>
                    <span class="menu-text">Quản Lý Giao Dịch </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="<#if active_admin_info??>active</#if>">
                        <a href="/transaction">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Danh Sách Giao Dịch
                        </a>
                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
        </#if>
            <#if transaction == true>
            <li class="active open">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-tag"></i>
                    <span class="menu-text">Quản Lý Tài Chính </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="<#if active_admin_info??>active</#if>">
                        <a href="/profits">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Doanh Thu
                        </a>
                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
            </#if>
        </ul>


        <!-- /.nav-list -->
    <#--<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">-->
    <#--<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state"-->
    <#--data-icon1="ace-icon fa fa-angle-double-left"-->
    <#--data-icon2="ace-icon fa fa-angle-double-right"></i>-->
    <#--</div>-->

    </div>