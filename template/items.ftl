<style type="text/css">
    .expireItem {
        color: red;
    }
</style>
<div class="page-content" style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
    <div class="page-header">
        <h1>
            Manage Item
            <small>
                <i class="ace-icon fa fa-angle-double-right"></i>
                Item List
            </small>
        </h1>
    </div>
    <!-- /.page-header -->
    <div class="row">

        <div class="col-xs-12">

            <div class="row">
                <div class="col-xs-12">

                <#--<div class="clearfix">-->
                <#--<div class="pull-right tableTools-container"></div>-->
                <#--</div>-->
                    <div class="table-header"
                         style="font-family: 'Arial Unicode MS'; text-transform: capitalize;">
                        Danh sách sản phẩm
                    </div>

                    <div>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>

                                <th style="width: 100px; text-align: center;">Mã</th>
                                <#if title==true>
                                    <th style="text-align: center;">Tiêu đề</th>
                                </#if>
                                <#if des==true>
                                    <th style="text-align: center;">Mô tả</th>
                                </#if>
                                <#if content==true>
                                    <th style="text-align: center;">Nội dung</th>
                                </#if>
                                <#if price == true>
                                    <th style="text-align: center; width: 100px;">Giá Tiền</th>
                                </#if>
                                <th style="text-align: center;width: 100px;">Hình ảnh</th>
                                <th style="text-align: center;width: 200px">Tên Danh Mục</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody style="font-family: 'Arial Unicode MS'; text-transform: none;">
                                  <#list itemList as item>
                                  <tr>

                                      <td style="width: 100px; text-align: center;">${item.id}</td>
                                       <#if title==true>
                                      <td style="width: 200px;text-align: center;">${item.title }
                                       </#if>
                                       <#if des==true>
                                      <td>${item.description}</td>
                                       </#if>
                                      <#if content==true>
                                          <td style="width: 100px;text-align: center;">
                                              <a href="item-content-detail?id=${item.id}" target="_blank">
                                                  <p> Xem Chi Tiết </p></a>
                                          </td>
                                      </#if>
                                      <#if price == true>
                                          <td style="text-align: center">${item.price}</td>
                                      </#if>

                                      <td style="width: 100px;text-align: center;">
                                          <a href="${upload_url}${item.imgUrl}" target="_blank">
                                              <img style="width: 40px; height: 40px" src="${upload_url}${item.imgUrl}"/>
                                          </a>
                                      </td>

                                      <td style="text-align: center">${item.project.name}</td>

                                      <td style="width: 100px;text-align: center;">
                                          <div class="hidden-sm hidden-xs action-buttons">
                                              <a class="red" href="/removeItem?id=${item.id}">
                                                  <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                              </a>
                                              <a class="red" href="/updateItem?id=${item.id}">
                                                  <i class="ace-icon fa fa-edit bigger-130"></i>
                                              </a>
                                          </div>
                                      </td>

                                  </tr>

                                  </#list>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="user-table_paginate">
                            <ul class="pagination">
                            <#if currentPage ==1>
                                    <li class="paginate_button previous disabled" aria-controls="user-table"
                                        tabindex="0">
                                        <a href="#">Previous</a>
                                    </li>
                            <#else >
                                     <li class="paginate_button previous " aria-controls="user-table" tabindex="0">
                                         <a href="/items?page=${prePage}">Previous</a>
                                     </li>
                            </#if>
                                <#assign x=page>
                                <#list 1..x as i>
                                     <li class="paginate_button active" aria-controls="user-table" tabindex="0">
                                         <a href="/items?page=${i}" style="border-color: #D9D9D9">${i}</a>
                                     </li>

                                </#list>
                            <#if currentPage ==page>
                                <li class="paginate_button next disabled" aria-controls="user-table" tabindex="0">
                                    <a href="#">Next</a>
                                </li>
                            <#else>
                                <li class="paginate_button next" aria-controls="user-table" tabindex="0">
                                    <a href="/items?page=${nextPage}">Next</a>
                                </li>
                            </#if>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- PAGE CONTENT ENDS -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</div>

<script type="text/javascript">
    let userTable =
            $('#user-table')
            //.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
                    .DataTable({
                        bAutoWidth: false,
                        "aoColumns": [
                            {"bSortable": false},
                            null, null, null, null,
                            {"bSortable": false}
                        ],
                        "aaSorting": [],
                        select: {
                            style: 'multi'
                        }
                    });

    $.fn.dataTable.Buttons.defaults.dom.container.className = 'dt-buttons btn-overlap btn-group btn-overlap';

    new $.fn.dataTable.Buttons(userTable, {
        buttons: [
            {
                "extend": "colvis",
                "text": "<i class='fa fa-search bigger-110 blue'></i> <span class='hidden'>Show/hide columns</span>",
                "className": "btn btn-white btn-primary btn-bold",
                columns: ':not(:first):not(:last)'
            },
            {
                "extend": "copy",
                "text": "<i class='fa fa-copy bigger-110 pink'></i> <span class='hidden'>Copy to clipboard</span>",
                "className": "btn btn-white btn-primary btn-bold"
            },
            {
                "extend": "csv",
                "text": "<i class='fa fa-database bigger-110 orange'></i> <span class='hidden'>Export to CSV</span>",
                "className": "btn btn-white btn-primary btn-bold"
            },
            {
                "extend": "excel",
                "text": "<i class='fa fa-file-excel-o bigger-110 green'></i> <span class='hidden'>Export to Excel</span>",
                "className": "btn btn-white btn-primary btn-bold"
            },
            {
                "extend": "pdf",
                "text": "<i class='fa fa-file-pdf-o bigger-110 red'></i> <span class='hidden'>Export to PDF</span>",
                "className": "btn btn-white btn-primary btn-bold"
            },
            {
                "extend": "print",
                "text": "<i class='fa fa-print bigger-110 grey'></i> <span class='hidden'>Print</span>",
                "className": "btn btn-white btn-primary btn-bold",
                autoPrint: false,
                message: 'This print was produced using the Print button for DataTables'
            }
        ]
    });
    userTable.buttons().container().appendTo($('.tableTools-container'));

    //style the message box
    let defaultCopyAction = userTable.button(1).action();
    userTable.button(1).action(function (e, dt, button, config) {
        defaultCopyAction(e, dt, button, config);
        $('.dt-button-info').addClass('gritter-item-wrapper gritter-info gritter-center white');
    });

    let defaultColvisAction = userTable.button(0).action();
    userTable.button(0).action(function (e, dt, button, config) {
        defaultColvisAction(e, dt, button, config);
        if ($('.dt-button-collection > .dropdown-menu').length == 0) {
            $('.dt-button-collection')
                    .wrapInner('<ul class="dropdown-menu dropdown-light dropdown-caret dropdown-caret" />')
                    .find('a').attr('href', '#').wrap("<li />")
        }
        $('.dt-button-collection').appendTo('.tableTools-container .dt-buttons')
    });

    ////

    setTimeout(function () {
        $($('.tableTools-container')).find('a.dt-button').each(function () {
            var div = $(this).find(' > div').first();
            if (div.length == 1) {
                div.tooltip({container: 'body', title: div.parent().text()});
            } else {
                $(this).tooltip({container: 'body', title: $(this).text()});
            }
        });
    }, 500);

    userTable.on('select', function (e, dt, type, index) {
        if (type === 'row') {
            $(userTable.row(index).node()).find('input:checkbox').prop('checked', true);
        }
    });
    userTable.on('deselect', function (e, dt, type, index) {
        if (type === 'row') {
            $(userTable.row(index).node()).find('input:checkbox').prop('checked', false);
        }
    });

    /////////////////////////////////
    //table checkboxes
    $('th input[type=checkbox], td input[type=checkbox]').prop('checked', false);

    //select/deselect all rows according to table header checkbox
    $('#user-table > thead > tr > th input[type=checkbox], #user-table_wrapper input[type=checkbox]').eq(
            0).on('click', function () {
        let th_checked = this.checked;//checkbox inside "TH" table header

        $('#user-table').find('tbody > tr').each(function () {
            let row = this;
            if (th_checked) {
                userTable.row(row).select();
            } else {
                userTable.row(row).deselect();
            }
        });
    });

    //select/deselect a row when the checkbox is checked/unchecked
    $('#user-table').on('click', 'td input[type=checkbox]', function () {
        let row = $(this).closest('tr').get(0);
        if (this.checked) {
            userTable.row(row).deselect();
        } else {
            userTable.row(row).select();
        }
    });

    // $(document).on('click', '#user-table #permission-table .dropdown-toggle', function (e) {
    //     e.stopImmediatePropagation();
    //     e.stopPropagation();
    //     e.preventDefault();
    // });

</script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<!-- page specific plugin scripts -->
<script src="assets/js/jquery.bootstrap-duallistbox.min.js"></script>
<script src="assets/js/jquery.raty.min.js"></script>
<script src="assets/js/bootstrap-multiselect.min.js"></script>
<script src="assets/js/select2.min.js"></script>
<script src="assets/js/jquery-typeahead.js"></script>
		  <script src="assets/js/excanvas.min.js"></script>
		<![endif]-->
		<script src="assets/js/jquery-ui.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/chosen.jquery.min.js"></script>
		<script src="assets/js/spinbox.min.js"></script>
		<script src="assets/js/bootstrap-datepicker.min.js"></script>
		<script src="assets/js/bootstrap-timepicker.min.js"></script>
		<script src="assets/js/moment.min.js"></script>
		<script src="assets/js/daterangepicker.min.js"></script>
		<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
		<script src="assets/js/bootstrap-colorpicker.min.js"></script>
		<script src="assets/js/jquery.knob.min.js"></script>
		<script src="assets/js/autosize.min.js"></script>
		<script src="assets/js/jquery.inputlimiter.min.js"></script>
		<script src="assets/js/jquery.maskedinput.min.js"></script>
		<script src="assets/js/bootstrap-tag.min.js"></script>

<!-- ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
    var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox(
            {infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
    var container1 = demo1.bootstrapDualListbox('getContainer');
    container1.find('.btn').addClass('btn-white btn-info btn-bold');

    /**var setRatingColors = function() {
					$(this).find('.star-on-png,.star-half-png').addClass('orange2').removeClass('grey');
					$(this).find('.star-off-png').removeClass('orange2').addClass('grey');
				}*/
    $('.rating').raty({
        'cancel': true,
        'half': true,
        'starType': 'i'
        /**,

         'click': function() {
						setRatingColors.call(this);
					},
         'mouseover': function() {
						setRatingColors.call(this);
					},
         'mouseout': function() {
						setRatingColors.call(this);
					}*/
    })//.find('i:not(.star-raty)').addClass('grey');

    //////////////////
    //select2
    $('.select2').css('width', '200px').select2({allowClear: true})
    $('#select2-multiple-style .btn').on('click', function (e) {
        var target = $(this).find('input[type=radio]');
        var which = parseInt(target.val());
        if (which == 2) {
            $('.select2').addClass('tag-input-style');
        } else {
            $('.select2').removeClass('tag-input-style');
        }
    });

    //////////////////
    $('.multiselect').multiselect({
        enableFiltering: true,
        enableHTML: true,
        buttonClass: 'btn btn-white btn-primary',
        templates: {
            button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"><span class="multiselect-selected-text"></span> &nbsp;<b class="fa fa-caret-down"></b></button>',
            ul: '<ul class="multiselect-container dropdown-menu"></ul>',
            filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
            filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
            li: '<li><a tabindex="0"><label></label></a></li>',
            divider: '<li class="multiselect-item divider"></li>',
            liGroup: '<li class="multiselect-item multiselect-group"><label></label></li>'
        }
    });

    ///////////////////

    //typeahead.js
    //example taken from plugin's page at: https://twitter.github.io/typeahead.js/examples/
    var substringMatcher = function (strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function (i, str) {
                if (substrRegex.test(str)) {
                    // the typeahead jQuery plugin expects suggestions to a
                    // JavaScript object, refer to typeahead docs for more info
                    matches.push({value: str});
                }
            });

            cb(matches);
        }
    }

    $('input.typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    }, {
        name: 'states',
        displayKey: 'value',
        source: substringMatcher(ace.vars['US_STATES']),
        limit: 10
    });

    ///////////////

    //in ajax mode, remove remaining elements before leaving page
    $(document).one('ajaxloadstart.page', function (e) {
        $('[class*=select2]').remove();
        $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox('destroy');
        $('.rating').raty('destroy');
        $('.multiselect').multiselect('destroy');
    });

    $("#itemType").on("change", function () {
        let itemTypes = $(this).multiselect().val();
        if (itemTypes != null) {
            $.get("/fitterItems", {
                data: JSON.stringify(itemTypes)
            }).done(function (data) {
                myTable.clear();
                let itemDatas = JSON.parse(data);
                for (let i in itemDatas) {
                    let it = itemDatas[i];
                    let i1 = "<div class=\"center\">\n"
                            + "                  <label class=\"pos-rel\">\n"
                            + "                    <input type=\"checkbox\" class=\"ace\"/>\n"
                            + "                    <span class=\"lbl\"></span>\n"
                            + "                  </label>\n"
                            + "                </div>";
                    let i2 = "<span>" + it.refId + "</span>";
                    if (it.quantity == 0) {
                        i2 = "<span style=\"color:red\">" + it.refId + "</span>";
                    }
                    let i3 = "<span>" + it.name + "</span>";
                    if (it.quantity == 0) {
                        i3 = "<span style=\"color:red\">" + it.name + "</span>";
                    }
                    let i4 = "<img style=\"width: 40px; height: 40px\" src=\"" + it.image + "\">";
                    let i5 = "<span>" + it.price + "</span>";
                    if (it.quantity == 0) {
                        i5 = "<span style=\"color:red\">" + it.price + "</span>";
                    }
                    let i6 = "<span>" + it.provider + "</span>";
                    if (it.quantity == 0) {
                        i6 = "<span style=\"color:red\">" + it.provider + "</span>";
                    }
                    let i7 = "<span>" + it.quantity + "</span>";
                    if (it.quantity == 0) {
                        i7 = "<span style=\"color:red\">" + it.quantity + "</span>";
                    }
                    let i8 = "<div class=\"hidden-sm hidden-xs action-buttons\"><a class=\"red\" href=\"/removeItem?id="
                            + it.id + "\">"
                            + "<i class=\"ace-icon fa fa-trash-o bigger-130\"></i></a></div>";
                    myTable.row.add([i1, i2, i3, i4, i5, i6, i7, i8]);
                }
                myTable.draw();
            });
        }
        console.log("Change: " + itemTypes);
    })
</script>